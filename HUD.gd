extends CanvasLayer


var file_to_display = ["res://nose.tscn", "res://World.tscn"]
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var timeRemaining = 30
var hairRemoved = 0
var gameActive = false

func _physics_process(delta):
	if not get_tree().current_scene.filename in file_to_display:
		$TimeRemainingLabel.visible = false
		$HairRemovedLabel.visible = false
	else:
		$TimeRemainingLabel.visible = true
		$HairRemovedLabel.visible = true


func _on_TimeRemainingTimer_timeout():
	if gameActive:
		timeRemaining -= 1
	if timeRemaining < 0:
		if hairRemoved >= 50:
			get_tree().change_scene("res://WinScreen.tscn")
		else:
			get_tree().change_scene("res://LoseScreen.tscn")
		gameActive = false
		timeRemaining = 30
		clear_hair_removed()
	else:
		if gameActive:
			update_time_remaining(timeRemaining)
	
func update_time_remaining(timeRemaining):
	$TimeRemainingLabel.text = "Time Remaining: " + str(timeRemaining) + " seconds"

func update_amount_hair_removed():
	hairRemoved += 0.3
	$HairRemovedLabel.text = "Hair Removed: " + str(ceil(hairRemoved))
	
func clear_hair_removed():
	hairRemoved = 0 
	$HairRemovedLabel.text = "Hair Removed: " + str(0)
	
