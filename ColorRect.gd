extends ColorRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const every_x = 20
var x = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	x+=1
	if x > every_x:
		color = color.from_hsv(rand_range(0, 350), rand_range(0, 359), rand_range(0,359))
		x = 0
