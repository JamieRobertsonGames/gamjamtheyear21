extends KinematicBody

export (int) var turn_speed = 10
export (int) var speed = 0
export (int) var maxSpeed = 500
export (int) var minSpeed = 100
export (int) var turn_multi = 15


var rot_x = 0.0
var rot_y = 0.0
var rot_z = 0.0

func get_input(delta):

	if Input.is_action_pressed("ui_right"):
		rot_y = -turn_speed * delta * turn_multi

	elif Input.is_action_pressed("ui_left"):
		rot_y = turn_speed * delta * turn_multi
	else:
		rot_y = 0
		
	if Input.is_action_pressed("ui_down"):
		rot_x = -turn_speed * delta * turn_multi
	elif Input.is_action_pressed("ui_up"):
		rot_x = turn_speed * delta * turn_multi
	else:
		rot_x = 0

func _physics_process(delta):
	get_input(delta)
	speed += 10

	rotate_object_local(Vector3(1, 0, 0), deg2rad(rot_x))
	rotate_object_local(Vector3(0, 1, 0), deg2rad(rot_y))
	rotate_object_local(Vector3(0, 0, 1), deg2rad(rot_z))

	if speed > 0:
		speed = min(speed, maxSpeed)
	elif speed < 0:
		speed = max(minSpeed, speed)

	move_and_slide(-transform.basis.z * speed * delta)
